from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.contrib.kubernetes.volume_mount import VolumeMount
from airflow.contrib.kubernetes.volume import Volume
from airflow.kubernetes.secret import Secret
from airflow import DAG
from airflow.utils.dates import days_ago

args = {
    "project_id": "untitled-0105174455",
}

dag = DAG(
    "untitled-0105174455",
    default_args=args,
    schedule_interval="@once",
    start_date=days_ago(1),
    description="""
Created with Elyra 3.15.0 pipeline editor using `untitled.pipeline`.
    """,
    is_paused_upon_creation=False,
)


# Operator source: datalab/duplicate-dedup.ipynb

op_cbf34193_817c_434c_8b29_6a5a8d095497 = KubernetesPodOperator(
    name="duplicate_dedup",
    namespace="airflow",
    image="coreplatformdevelopment.azurecr.io/elyra-runtime-image",
    image_pull_secrets="coreplatformdevelopment-acrpull",
    cmds=["sh", "-c"],
    arguments=[
        "mkdir -p ./jupyter-work-dir/ && cd ./jupyter-work-dir/ && echo 'Downloading https://raw.githubusercontent.com/elyra-ai/elyra/v3.15.0/elyra/airflow/bootstrapper.py' && curl --fail -H 'Cache-Control: no-cache' -L https://raw.githubusercontent.com/elyra-ai/elyra/v3.15.0/elyra/airflow/bootstrapper.py --output bootstrapper.py && echo 'Downloading https://raw.githubusercontent.com/elyra-ai/elyra/v3.15.0/etc/generic/requirements-elyra.txt' && curl --fail -H 'Cache-Control: no-cache' -L https://raw.githubusercontent.com/elyra-ai/elyra/v3.15.0/etc/generic/requirements-elyra.txt --output requirements-elyra.txt && python3 -m pip install packaging && python3 -m pip freeze > requirements-current.txt && python3 bootstrapper.py --pipeline-name 'untitled' --cos-endpoint http://192.168.1.16 --cos-bucket airflow --cos-directory 'untitled-0105174455' --cos-dependencies-archive 'duplicate-dedup-cbf34193-817c-434c-8b29-6a5a8d095497.tar.gz' --file 'datalab/duplicate-dedup.ipynb' "
    ],
    task_id="duplicate_dedup",
    env_vars={
        "ELYRA_RUNTIME_ENV": "airflow",
        "AWS_ACCESS_KEY_ID": "FlIO0mzdIDxq3m0K4XNWoSj",
        "AWS_SECRET_ACCESS_KEY": "SOUY8yNvhEwT2rS64gxivcutf4XZ9b8rMymUnI",
        "ELYRA_ENABLE_PIPELINE_INFO": "True",
        "ELYRA_RUN_NAME": "untitled-{{ ts_nodash }}",
    },
    volumes=[],
    volume_mounts=[],
    secrets=[],
    annotations={},
    labels={},
    tolerations=[],
    in_cluster=True,
    config_file="None",
    dag=dag,
)

op_cbf34193_817c_434c_8b29_6a5a8d095497.image_pull_policy = "Always"
